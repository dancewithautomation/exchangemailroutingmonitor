﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ServiceModel;

namespace AgentCommon
{
    [ServiceKnownType(typeof(MailItemData))]
    public interface IAgentCallback
    {
        [OperationContract(IsOneWay = true)]
        void Delete();

        [OperationContract(IsOneWay = false)]
        void GetMimeContent(out string content);
        [OperationContract(IsOneWay = false)]
        string GetSubject();

        [OperationContract(IsOneWay = true)]
        void ScanCompleted();
        [OperationContract(IsOneWay = false)]
        bool IsCompleted();
        [OperationContract(IsOneWay = false)]
        string GetEventName();
        [OperationContract(IsOneWay = false)]
        HookPoint GetEventType();
        [OperationContract(IsOneWay = false)]
        void AddHeader(string name, string value);
        [OperationContract(IsOneWay = true)]
        void Fork();
        [OperationContract(IsOneWay = false)]
        MailItemData GetMailItemData();


        [OperationContract(IsOneWay = false)]
        List<string> GetRecipients();
        [OperationContract(IsOneWay = false)]
        void RemoveRecipient(string rcpt);
    }

    public interface IAgentBaseCallback2
    {
        [OperationContract(IsOneWay = false)]
        void GetMimeContent(out string content);
        [OperationContract(IsOneWay = false)]
        string GetSubject();
    }

    public interface IAgentBaseCallback
    {
        [OperationContract(IsOneWay = true)]
        void ScanCompleted();
        [OperationContract(IsOneWay = false)]
        bool IsCompleted();
        [OperationContract(IsOneWay = false)]
        string GetEventName();
    }

    [ServiceKnownType(typeof(SmtpSessionData))]
    public interface IAgentSmtpCallback1 
    {
        [OperationContract(IsOneWay = true)]
        void ScanCompleted();
        [OperationContract(IsOneWay = false)]
        bool IsCompleted();
        [OperationContract(IsOneWay = false)]
        string GetEventName();
        [OperationContract(IsOneWay = false)]
        HookPoint GetEventType();
        [OperationContract(IsOneWay = true)]
        void DisConnect();
        [OperationContract(IsOneWay = false)]
        bool IsConnected();
        [OperationContract(IsOneWay = false)]
        bool IsExternalConnection();
        [OperationContract(IsOneWay = false)]
        string GetLastExternalIp();
        [OperationContract(IsOneWay = true)]
        void RejectConnection();
        [OperationContract(IsOneWay = false)]
        SmtpSessionData GetSessionData();

    }

    [ServiceKnownType(typeof(SmtpSessionData))]
    public interface IAgentSmtpCallback2
    {

        [OperationContract(IsOneWay = false)]
        void GetMimeContent(out string content);
        [OperationContract(IsOneWay = true)]
        void ScanCompleted();
        [OperationContract(IsOneWay = false)]
        bool IsCompleted();
        [OperationContract(IsOneWay = false)]
        string GetEventName();
        [OperationContract(IsOneWay = false)]
        HookPoint GetEventType();
        [OperationContract(IsOneWay = true)]
        void DisConnect();
        [OperationContract(IsOneWay = false)]
        bool IsConnected();
        [OperationContract(IsOneWay = false)]
        bool IsExternalConnection();
        [OperationContract(IsOneWay = false)]
        string GetLastExternalIp();
        [OperationContract(IsOneWay = true)]
        void RejectConnection();
        [OperationContract(IsOneWay = false)]
        SmtpSessionData GetSessionData();
    }
    
    [ServiceKnownType(typeof(SmtpSessionData))]
    [ServiceKnownType(typeof(MailItemData))]
    public interface IAgentSmtpCallback3 
    {
        [OperationContract(IsOneWay = true)]
        void ScanCompleted();
        [OperationContract(IsOneWay = false)]
        bool IsCompleted();
        [OperationContract(IsOneWay = false)]
        string GetEventName();
        [OperationContract(IsOneWay = false)]
        HookPoint GetEventType();
        [OperationContract(IsOneWay = true)]
        void DisConnect();
        [OperationContract(IsOneWay = false)]
        bool IsConnected();
        [OperationContract(IsOneWay = false)]
        bool IsExternalConnection();
        [OperationContract(IsOneWay = false)]
        string GetLastExternalIp();
        [OperationContract(IsOneWay = false)]
        void RemoveRecipient(string rcpt);
        [OperationContract(IsOneWay = true)]
        void RejectMessage();


        [OperationContract(IsOneWay = false)]
        string GetSubject();

        [OperationContract(IsOneWay = false)]
        SmtpSessionData GetSessionData();

        [OperationContract(IsOneWay = false)]
        MailItemData GetMailItemData();

        [OperationContract(IsOneWay = false)]
        List<string> GetRecipients();
        [OperationContract(IsOneWay = false)]
        void GetMimeContent(out string content);
    }

    public enum HookPoint
    { 
        OnSmtpConnect,
        OnSmtpDisConnected,
        OnSmtpEndOfData,
        OnSmtpEndOfHeader,
        OnSmtpRcptCommand,
        OnSmtpReject,
        OnSubmittedMessage,
        OnResolvedMessage,
        OnRoutedMessage,
        OnCategorizedMessage
    }

    public enum AgentCallbackInterfaceTypes
    {
        IAgentSmtpCallback3,
        IAgentSmtpCallback2,
        IAgentSmtpCallback1,
        IAgentCallback
    }
}
