﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace AgentCommon
{
    [DataContract]
    public class MailItemData
    {
        [DataMember]
        public string Subject { get; set; }

        [DataMember]
        public string Body { get; set; }

        [DataMember]
        public string MessageId { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public string Charset { get; set; }

        [DataMember]
        public string HeaderDecodingCodeName { get; set; }

        [DataMember]
        public string FromAddress { get; set; }

        [DataMember]
        public List<string> Recipients { get; set; }

        [DataMember]
        public List<string> Headers { get; set; }

        [DataMember]
        public string EnvelopeId { get; set; }

        [DataMember]
        public string DateTimeReceived { get; set; }

        [DataMember]
        public string DeliveryPriority { get; set; }

        [DataMember]
        public string DsnFormatRequested { get; set; }

        [DataMember]
        public string InboundDeliveryMethod { get; set; }

        [DataMember]
        public long MimeStreamLength { get; set; }

        [DataMember]
        public bool MustDeliver { get; set; }

        [DataMember]
        public string OriginalAuthenticator { get; set; }

        [DataMember]
        public string OriginatingDomain { get; set; }

        [DataMember]
        public string OriginatorOrganization { get; set; }

        [DataMember]
        public string Properties { get; set; }

        [DataMember]
        public string TenantId { get; set; }

        [DataMember]
        public bool IsInterpersonalMessage { get; set; }

        [DataMember]
        public bool IsOpaqueMessage { get; set; }

        [DataMember]
        public bool IsSystemMessage { get; set; }

        [DataMember]
        public string MapiMessageClass { get; set; }

        [DataMember]
        public string MessageSecurityType { get; set; }

        [DataMember]
        public string Sender { get; set; }

        [DataMember]
        public string From { get; set; }

        [DataMember]
        public string To { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("MessageId:");
            sb.AppendLine(MessageId==null?"NULL":MessageId);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("EnvelopeId:");
            sb.AppendLine(EnvelopeId == null ? "NULL" : EnvelopeId);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("TenantId:");
            sb.AppendLine(TenantId == null ? "NULL" : TenantId);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("FromAddress:");
            sb.AppendLine(FromAddress == null ? "NULL" : FromAddress);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("Recipients:");
            if (Recipients != null)
            {
                foreach (var v in Recipients)
                {
                    sb.AppendLine(v);
                }
            }
            sb.AppendLine("---------------------------------");
            sb.AppendLine("DateTimeReceived:");
            sb.AppendLine(DateTimeReceived == null ? "NULL" : DateTimeReceived);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("DeliveryPriority:");
            sb.AppendLine(DeliveryPriority == null ? "NULL" : DeliveryPriority);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("DsnFormatRequested:");
            sb.AppendLine(DsnFormatRequested == null ? "NULL" : DsnFormatRequested);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("InboundDeliveryMethod:");
            sb.AppendLine(InboundDeliveryMethod == null ? "NULL" : InboundDeliveryMethod);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("MimeStreamLength:");
            sb.AppendLine(MimeStreamLength.ToString());
            sb.AppendLine("---------------------------------");
            sb.AppendLine("MustDeliver:");
            sb.AppendLine(MustDeliver.ToString());
            sb.AppendLine("---------------------------------");
            sb.AppendLine("OriginalAuthenticator:");
            sb.AppendLine(OriginalAuthenticator == null ? "NULL" : OriginalAuthenticator);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("OriginatingDomain:");
            sb.AppendLine(OriginatingDomain == null ? "NULL" : OriginatingDomain);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("OriginatorOrganization:");
            sb.AppendLine(OriginatorOrganization == null ? "NULL" : OriginatorOrganization);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("Properties:");
            sb.AppendLine(Properties == null ? "NULL" : Properties);
            sb.AppendLine("---------------------------------");

            sb.AppendLine("The follow are from EmailMessage");
            sb.AppendLine("From:");
            sb.AppendLine(From == null ? "NULL" : From);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("To:");
            sb.AppendLine(To == null ? "NULL" : To);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("Sender:");
            sb.AppendLine(Sender == null ? "NULL" : Sender);
            sb.AppendLine("---------------------------------");


            sb.AppendLine("IsInterpersonalMessage:");
            sb.AppendLine(IsInterpersonalMessage.ToString());
            sb.AppendLine("---------------------------------");
            sb.AppendLine("IsOpaqueMessage:");
            sb.AppendLine(IsOpaqueMessage.ToString());
            sb.AppendLine("---------------------------------");
            sb.AppendLine("IsSystemMessage:");
            sb.AppendLine(IsSystemMessage.ToString());
            sb.AppendLine("---------------------------------");
            sb.AppendLine("MapiMessageClass:");
            sb.AppendLine(MapiMessageClass == null ? "NULL" : MapiMessageClass);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("MessageSecurityType:");
            sb.AppendLine(MessageSecurityType == null ? "NULL" : MessageSecurityType);
            sb.AppendLine("---------------------------------");


            sb.AppendLine("ContentType:");
            sb.AppendLine(ContentType == null ? "NULL" : ContentType);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("HeaderDecodingCodeName:");
            sb.AppendLine(HeaderDecodingCodeName==null?"NULL":HeaderDecodingCodeName);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("Charset:");
            sb.AppendLine(Charset==null?"NULL":Charset);
            sb.AppendLine("---------------------------------");
            sb.AppendLine("Headers:");
            if (Headers != null)
            {
                foreach (var v in Headers)
                {
                    sb.AppendLine(v);
                }
            }
            sb.AppendLine("---------------------------------");
            sb.AppendLine("Body:");
            if (Body != null)
            {
                sb.AppendLine(Body.TrimEnd());
            }
            
            return sb.ToString();
        }
    }
}
