﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace AgentCommon
{

    //public delegate void MyCallBack();

    [DataContract]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class MailItemWrapper:IAgentCallback
    {
        public string Subject()
        {
            return null;
        }

        private MailItem _mailItem;
        //[DataMember]
        //public MyCallBack CB { get; set; }
        //[DataMember]
        //public MailItem MailItem
        //{
        //    get { return _mailItem; }
        //    set { _mailItem = value; }
        //}
        //public WaitCallBack
        private AgentAsyncContext _agentAsyncContext;
        //[DataMember]
        //public AgentAsyncContext AgentAsyncContext
        //{
        //    get { return _agentAsyncContext; }
        //    set { _agentAsyncContext = value; }
        //}
        private bool _isScanCompleted = false;
        [DataMember]
        public bool IsScanCompleted
        {
            get { return _isScanCompleted; }
            set { _isScanCompleted = value; }
        }
        
        public MailItemWrapper()
        { }

        public MailItemWrapper(AgentAsyncContext ac,MailItem mailItem)
        {
            _agentAsyncContext = ac;
            _mailItem = mailItem;
        }

        public void GetMimeStream(out Stream stream)
        {
            stream = _mailItem.GetMimeReadStream();
        }

        public void SetMimeStream(Stream stream)
        {
            try
            {
                using (Stream writeStream = _mailItem.GetMimeWriteStream())
                {
                    int bytesRead = 0;
                    byte[] buffer = new byte[4000];
                    while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        writeStream.Write(buffer, 0, bytesRead);
                    }
                    writeStream.Flush();
                }
            }
            finally
            {
                stream.Close();
            }
        }

        public void ScanCompleted()
        {
            if (_agentAsyncContext != null)
            {
                _agentAsyncContext.Complete();
                _agentAsyncContext = null;
                _isScanCompleted = true;
            }
        }

        public bool IsCompleted()
        {
            return _isScanCompleted;
        }
    }
}
