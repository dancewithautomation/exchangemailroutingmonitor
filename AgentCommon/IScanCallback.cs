﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Microsoft.Exchange.Data.Transport.Routing;

namespace AgentCommon
{
    [ServiceContract(SessionMode = SessionMode.Required,
                    CallbackContract = typeof(IAgentCallback))]
    public interface IScanCallback
    {
        [OperationContract]
        void Append2ScanQueue(AgentCallbackInterfaceTypes state);
        [OperationContract]
        void Log(string log);
        [OperationContract]
        bool IsForkedNeed(HookPoint hp);
    }

    [ServiceContract(SessionMode = SessionMode.Required,
                    CallbackContract = typeof(IAgentSmtpCallback1))]
    public interface IScanCallback2
    {
        [OperationContract]
        void Append2ScanQueue(AgentCallbackInterfaceTypes state);
        [OperationContract]
        void Log(string log);
    }

    [ServiceContract(SessionMode = SessionMode.Required,
                   CallbackContract = typeof(IAgentSmtpCallback2))]
    public interface IScanCallback3 
    {
        [OperationContract]
        void Append2ScanQueue(AgentCallbackInterfaceTypes state);
        [OperationContract]
        void Log(string log);
    }

    [ServiceContract(SessionMode = SessionMode.Required,
                CallbackContract = typeof(IAgentSmtpCallback3))]
    public interface IScanCallback4 
    {
        [OperationContract]
        void Append2ScanQueue(AgentCallbackInterfaceTypes state);
        [OperationContract]
        void Log(string log);
    }
}
