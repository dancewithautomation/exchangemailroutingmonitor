﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace AgentCommon
{
    [DataContract]
    public class SmtpSessionData
    {
        [DataMember]
        public string LastExternalIp { get; set; }

        [DataMember]
        public string LocalEndPoint { get; set; }

        [DataMember]
        public string RemoteEndPoint { get; set; }

        [DataMember]
        public bool IsConnected { get; set; }

        [DataMember]
        public bool IsExternalConnection { get; set; }

        [DataMember]
        public long SessionId { get; set; }

        [DataMember]
        public bool AntispamBypass { get; set; }

        [DataMember]
        public string AuthenticationSource { get; set; }

        [DataMember]
        public string HelloDomain { get; set; }

        [DataMember]
        public string Properties { get; set; }

        [DataMember]
        public bool IsTls { get; set; }

        [DataMember]
        public string OtherInfo { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SessionId:"+SessionId.ToString());
            sb.AppendLine("IsConnected:" + IsConnected.ToString());
            sb.AppendLine("IsTls:" + IsTls.ToString());
            sb.AppendLine("AntispamBypass:" + AntispamBypass.ToString());
            sb.AppendLine("AuthenticationSource:" + AuthenticationSource);
            sb.AppendLine("HelloDomain:" + HelloDomain);
            sb.AppendLine("IsExternalConnection:" + IsExternalConnection.ToString());
            sb.AppendLine("LastExternalIp:" + LastExternalIp);
            sb.AppendLine("LocalEndPoint:" + LocalEndPoint);
            sb.AppendLine("RemoteEndPoint:" + RemoteEndPoint);
            sb.AppendLine("Properties:" + Properties);
            sb.AppendLine("OtherInfo:" + OtherInfo);
            return sb.ToString();
        }
    }
}
