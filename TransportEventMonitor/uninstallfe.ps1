#$EXDIR="C:\Program Files\Microsoft\Exchange Server\V15\TransportRoles\agents"
Net Stop MSExchangeTransport
Net Stop MSExchangeFrontEndTransport
Write-Output "Disabling Agent..."
Disable-TransportAgent -Identity "AgentOnSmtpConnect" -Confirm:$false -TransportService FrontEnd
Disable-TransportAgent -Identity "AgentOnSmtpRCPT" -Confirm:$false -TransportService FrontEnd
Disable-TransportAgent -Identity "AgentOnEndOfHeaders" -Confirm:$false -TransportService FrontEnd
Disable-TransportAgent -Identity "AgentOnSmtpEndOfData" -Confirm:$false -TransportService FrontEnd

Write-Output "Uninstalling Agent.."
Uninstall-TransportAgent -Identity "AgentOnSmtpConnect" -Confirm:$false -TransportService FrontEnd
Uninstall-TransportAgent -Identity "AgentOnSmtpRCPT" -Confirm:$false -TransportService FrontEnd
Uninstall-TransportAgent -Identity "AgentOnEndOfHeaders" -Confirm:$false -TransportService FrontEnd
Uninstall-TransportAgent -Identity "AgentOnSmtpEndOfData" -Confirm:$false -TransportService FrontEnd


Net Start MsExchangeTransport
Net Start MSExchangeFrontEndTransport
Write-Output "Uninstall Complete."
