﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AgentCommon;
using System.ServiceModel;
using System.Collections.ObjectModel;

namespace TransportEventMonitor
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ScanEntry : IScanCallback, IScanCallback2, IScanCallback3, IScanCallback4, IDisposable
    {
        public static ScannerObservableCollection Scanners = new ScannerObservableCollection();
        public static ObservableCollection<string> DebugLogs = new ObservableCollection<string>();
        //private static object _lockRequest;
        public static bool NeedForkAtResolved = false;
        public static bool NeedForkAtRouted = false;
        public static bool NeedForkAtCategorized = false;
        public static bool NeedForkAtSubmitted = false;

        public static bool NeedBreakAtSmtpConnect = false;
        public static bool NeedBreakAtSmtpRCPT = false;
        public static bool NeedBreakAtSmtpEndOfHeader = false;
        public static bool NeedBreakAtSmtpEndOfData = false;
        public static bool NeedBreakAtResolved = false;
        public static bool NeedBreakAtRouted = false;
        public static bool NeedBreakAtCategorized = false;
        public static bool NeedBreakAtSubmitted = false;

        public delegate void ScannerDelegate(Scanner s);
        public ScannerDelegate OnNewScanner { get; set; }

        public ScanEntry()
        {
            //Scanners = new ScannerObservableCollection();
            // _lockRequest = new object();
        }

        //TODO: May need object lock
        public void Append2ScanQueue(AgentCallbackInterfaceTypes state)
        {
            try
            {
                Scanner newScanner = null;
                switch (state)
                {
                    case AgentCallbackInterfaceTypes.IAgentSmtpCallback1:
                        IAgentSmtpCallback1 ac = OperationContext.Current.GetCallbackChannel<IAgentSmtpCallback1>();
                        newScanner = ScannerFactory.CreateScanner(ac, state);
                        //Scanners.Add(ScannerFactory.CreateScanner(ac, state));
                        //ac.ScanCompleted();
                        break;
                    case AgentCallbackInterfaceTypes.IAgentSmtpCallback2:
                        IAgentSmtpCallback2 ac2 = OperationContext.Current.GetCallbackChannel<IAgentSmtpCallback2>();
                        newScanner = ScannerFactory.CreateScanner(ac2, state);
                        //Scanners.Add(ScannerFactory.CreateScanner(ac2, state));
                        //ac2.ScanCompleted();
                        break;
                    case AgentCallbackInterfaceTypes.IAgentSmtpCallback3:
                        IAgentSmtpCallback3 ac3 = OperationContext.Current.GetCallbackChannel<IAgentSmtpCallback3>();
                        newScanner = ScannerFactory.CreateScanner(ac3, state);
                        //Scanners.Add(ScannerFactory.CreateScanner(ac3, state));
                        //ac3.ScanCompleted();
                        break;
                    case AgentCallbackInterfaceTypes.IAgentCallback:
                        IAgentCallback ac4 = OperationContext.Current.GetCallbackChannel<IAgentCallback>();
                        newScanner = ScannerFactory.CreateScanner(ac4, state);
                        //Scanners.Add(ScannerFactory.CreateScanner(ac4, state));
                        //ac4.ScanCompleted();
                        break;
                    default:
                        throw new ArgumentException("cannot identify callback interface type");
                }

                Scanners.Add(newScanner);
                //check if need to Complete Automatically
                HookPoint hp = newScanner.AgentHookPoint;
                bool needBreak =
                    (hp == HookPoint.OnSmtpConnect && NeedBreakAtSmtpConnect) ||
                    (hp == HookPoint.OnSmtpRcptCommand && NeedBreakAtSmtpRCPT) ||
                    (hp == HookPoint.OnSmtpEndOfHeader && NeedBreakAtSmtpEndOfHeader) ||
                    (hp == HookPoint.OnSmtpEndOfData && NeedBreakAtSmtpEndOfData) ||
                    (hp == HookPoint.OnSubmittedMessage && NeedBreakAtSubmitted) ||
                    (hp == HookPoint.OnResolvedMessage && NeedBreakAtResolved) ||
                    (hp == HookPoint.OnRoutedMessage && NeedBreakAtRouted) ||
                    (hp == HookPoint.OnCategorizedMessage && NeedBreakAtCategorized);
                if (!needBreak)
                {
                    //newScanner.ScanComplete();
                    //if (hp == HookPoint.OnSmtpConnect)
                    //{
                    //    SmtpSessionData data = (newScanner as SmtpConnectScanner).SessionData;
                    //}
                    newScanner.ScanComplete();
                }
            }
            catch (Exception ex)
            {
                Log(ex.ToString());
            }
           
        }



        public void Log(string log)
        {
            Console.WriteLine(log);
            DebugLogs.Add(log);
        }


        public bool IsForkedNeed(HookPoint hp)
        {
            switch (hp)
            {
                case HookPoint.OnCategorizedMessage:
                    return NeedForkAtCategorized;
                case HookPoint.OnResolvedMessage:
                    return NeedForkAtResolved;
                case HookPoint.OnRoutedMessage:
                    return NeedForkAtRouted;
                case HookPoint.OnSubmittedMessage:
                    return NeedForkAtSubmitted;
                default:
                    return false;
            }
        }

        public void Dispose()
        {
            //Scanners.CollectionChanged -= new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_scanners_CollectionChanged);
        }
    }
}
