﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using AgentCommon;
using System.Windows.Controls;

namespace TransportEventMonitor
{
    public class ScannerListItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SmtpConnectDataTemplate { get; set; }
        public DataTemplate SmtpRCPTDataTemplate { get; set; }
        public DataTemplate SmtpEndDataTemplate { get; set; }
        public DataTemplate TransportDataTemplate { get; set; }

        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {

            if (item is SubmittedMessageScannger || item is ResolvedMessageScannger || 
                item is RoutedMessageScannger || item is CategorizedMessageScannger)
            {
                return TransportDataTemplate;
            }
            else if (item is SmtpConnectScanner)
            {
                return SmtpConnectDataTemplate;
            }
            else if (item is SmtpRcptCommandScanner)
            {
                return SmtpRCPTDataTemplate;
            }
            else if (item is EndOfDataScanner || item is EndOfHeaderScanner)
            {
                return SmtpEndDataTemplate;
            }
            else
            {
                return null;
            }
        }
    }
}
