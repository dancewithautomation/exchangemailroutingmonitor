﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.ServiceModel;
using AgentCommon;
using System.IO;

namespace TransportEventMonitor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ServiceHost host;

        
        public void StartService()
        {
            try
            {
                host = new ServiceHost(
                typeof(ScanEntry),
                new Uri[]{
                new Uri("net.pipe://localhost")});
                NetNamedPipeBinding binding = new NetNamedPipeBinding();
                host.AddServiceEndpoint(typeof(IScanCallback),
                binding,
                "ScanEntry");

                host.AddServiceEndpoint(typeof(IScanCallback2),
                  new NetNamedPipeBinding(),
                  "ScanEntry2");

                host.AddServiceEndpoint(typeof(IScanCallback3),
                   new NetNamedPipeBinding(),
                   "ScanEntry3");

                host.AddServiceEndpoint(typeof(IScanCallback4),
                   new NetNamedPipeBinding(),
                   "ScanEntry4");
                host.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
            
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            //update powershell script 
            string startUpLocation = Environment.CurrentDirectory;
            string installScript = startUpLocation + "\\install.ps1";
            //string uninstallScript = startUpLocation + "\\uninstall.ps1";
            string[] installScriptContent=File.ReadAllLines(installScript);
            for(int i=0;i<installScriptContent.Length;i++)
            {
                if(installScriptContent[i].StartsWith(@"$EXDIR="))
                {
                    installScriptContent[i]="$EXDIR=\""+startUpLocation+"\"";
                }
            }

            File.WriteAllLines(installScript, installScriptContent);

        }

        public void StopService()
        {
            try
            {
                if (host != null)
                {
                    host.Close();
                }
            }
            catch
            { }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            StopService();
            base.OnExit(e);
        }
    }
}
