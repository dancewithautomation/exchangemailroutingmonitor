﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TransportEventMonitor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ScannerObservableCollection _scanners = new ScannerObservableCollection(); 
        public MainWindow()
        {
            InitializeComponent();
            InitScannerEvent();
        }

        private void InitScannerEvent()
        {
            ScanEntry.Scanners.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Scanners_CollectionChanged);
            ScanEntry.DebugLogs.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(DebugLogs_CollectionChanged);
            ScannerListBox.ItemsSource = _scanners;
        }

        void DebugLogs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (object o in e.NewItems)
                {
                    Log(o.ToString());
                }
            }
        }

        void Log(string s)
        {
            DebugLogRichTextBox.AppendText(s);
            DebugLogRichTextBox.AppendText(Environment.NewLine);
            DebugLogRichTextBox.ScrollToEnd();
        }

        void Scanners_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            try
            {
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    foreach (object o in e.NewItems)
                    {
                        _scanners.Add(o as Scanner);
                    }
                }
                else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    foreach (object o in e.OldItems)
                    {
                        (o as Scanner).ScanComplete();
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex.ToString());
            }
           
        }

        private void CompleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = e.Source as Button;
                Scanner scanner = button.DataContext as Scanner;
                if (!scanner.IsCompleted)
                {
                    scanner.ScanComplete();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
            //MessageBox.Show(scanner.IsCompleted.ToString());
        }

        private void GoAllButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Scanner scanner in _scanners)
                {
                    scanner.ScanComplete();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            _scanners.Clear();
            ScanEntry.DebugLogs.Clear();
            ScanEntry.Scanners.Clear();
            DebugLogRichTextBox.Document.Blocks.Clear();
        }

        private void ForkAtMessageSubmittedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedForkAtSubmitted = item.IsChecked;
        }

        private void ForkAtMessageResolvedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedForkAtResolved = item.IsChecked;
        }

        private void ForkAtMessageRoutedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedForkAtRouted = item.IsChecked;
        }

        private void ForkAtMessageCategorizedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedForkAtCategorized = item.IsChecked;
        }

        private void ForkAtNoneMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ScanEntry.NeedForkAtSubmitted = false;
            ScanEntry.NeedForkAtResolved = false;
            ScanEntry.NeedForkAtRouted = false;
            ScanEntry.NeedForkAtCategorized = false;
            ForkAtMessageSubmittedMenuItem.IsChecked = false;
            ForkAtMessageResolvedMenuItem.IsChecked = false;
            ForkAtMessageRoutedMenuItem.IsChecked = false;
            ForkAtMessageCategorizedMenuItem.IsChecked = false;
        }

        private void BreakAtNoneMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ScanEntry.NeedBreakAtSmtpConnect = false;
            ScanEntry.NeedBreakAtSmtpRCPT = false;
            ScanEntry.NeedBreakAtSmtpEndOfHeader = false;
            ScanEntry.NeedBreakAtSmtpEndOfData = false;
            ScanEntry.NeedBreakAtSubmitted = false;
            ScanEntry.NeedBreakAtResolved = false;
            ScanEntry.NeedBreakAtRouted = false;
            ScanEntry.NeedBreakAtCategorized = false;
            BreakAtSmtpConnectMenuItem.IsChecked = false;
            BreakAtSmtpEndOfHeaderMenuItem.IsChecked = false;
            BreakAtSmtpEndOfDataMenuItem.IsChecked = false;
            BreakAtMessageSubmittedMenuItem.IsChecked = false;
            BreakAtMessageResolvedMenuItem.IsChecked = false;
            BreakAtMessageRoutedMenuItem.IsChecked = false;
            BreakAtMessageCategorizedMenuItem.IsChecked = false;
        }

        private void BreakAtSmtpConnectMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedBreakAtSmtpConnect = item.IsChecked;
        }

        private void BreakAtSmtpEndOfDataMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedBreakAtSmtpEndOfData = item.IsChecked;
        }

        private void BreakAtMessageSubmittedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedBreakAtSubmitted = item.IsChecked;
        }

        private void BreakAtMessageResolvedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedBreakAtResolved = item.IsChecked;
        }

        private void BreakAtMessageRoutedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedBreakAtRouted = item.IsChecked;
        }

        private void BreakAtMessageCategorizedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedBreakAtCategorized = item.IsChecked;
        }

        private void BreakAtSmtpEndOfHeaderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedBreakAtSmtpEndOfHeader = item.IsChecked;
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = sender as Button;
                SmtpAgentScannerBase scanner = button.DataContext as SmtpAgentScannerBase;
                scanner.Disconnect();
                scanner.ScanComplete();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RejectConnectionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = sender as Button;

                if (button.DataContext is SmtpConnectScanner)
                {
                    var scanner = button.DataContext as SmtpConnectScanner;
                    scanner.RejectConnection();
                    scanner.ScanComplete();
                }
                else
                {
                    var scanner = button.DataContext as SmtpRcptCommandScanner;
                    scanner.RejectConnection();
                    scanner.ScanComplete();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DeleteMessageButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = sender as Button;
                TansportAgentScannerBase scanner = button.DataContext as TansportAgentScannerBase;
                scanner.Delete();
                if (!scanner.IsCompleted)
                {
                    scanner.ScanComplete();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RejectMessageButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = sender as Button;
                SmtpAgentScannerBase scanner = button.DataContext as SmtpAgentScannerBase;
                scanner.RejectMessage();
                scanner.ScanComplete();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RemoveRecipientButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = sender as Button;
                ComboBox box=(button.Parent as FrameworkElement).FindName("RecipientsComboBox") as ComboBox;
                //button.tem
                Scanner scanner = button.DataContext as Scanner;
                scanner.RemoveRecipient(box.SelectedItem.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void StartServiceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                (App.Current as App).StartService();
                CommunicationStatusBar.Text = "Communication Ready";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BreakAtSmtpRCPTMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ScanEntry.NeedBreakAtSmtpRCPT = item.IsChecked;
        }
    }
}
