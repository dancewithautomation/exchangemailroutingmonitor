﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AgentCommon;
using System.IO;
using System.ServiceModel;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace TransportEventMonitor
{

    public abstract class Scanner : IDisposable, INotifyPropertyChanged
    {
        public HookPoint AgentHookPoint { get; protected set; }
        //protected string _description;
        public virtual string Description 
        {
            get
            {
                return AgentHookPoint.ToString()+",Complete Status:"+IsCompleted.ToString();
            }
        }

        protected List<string> _recipients = new List<string>();
        protected abstract void GetRecipients();

        public virtual ObservableCollection<string> Recipients
        {
            get
            {
                if (_recipients == null || _recipients.Count==0)
                {
                    GetRecipients();
                }
                return new ObservableCollection<string>(_recipients); ;
            }
        }

        public abstract void RemoveRecipient(string rcpt);

        public virtual bool IsCompleted
        {
            get
            {
                return false;
            }
        }

        public Scanner()
        {
        }

        public virtual void ScanComplete()
        {
            RaisePropertyChanged("IsCompleted");
        }

        public void Dispose()
        {
            ScanComplete();
        }

        #region INotifyPropertyChanged Members

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string PropertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }

        #endregion
    }

    public abstract class SmtpAgentScannerBase : Scanner
    {
        public abstract SmtpSessionData SessionData { get; }

        public abstract void Disconnect();

        public virtual void RejectMessage()
        { }
    }

    public class EndOfDataScanner : SmtpAgentScannerBase
    {
        protected IAgentSmtpCallback3 AgentCallBack { get; set; }

        public EndOfDataScanner(IAgentSmtpCallback3 callback)
        {
            AgentCallBack = callback;
            AgentHookPoint = HookPoint.OnSmtpEndOfData;
        }

        public override void ScanComplete()
        {
            if (AgentCallBack != null && !AgentCallBack.IsCompleted())
            {
                AgentCallBack.ScanCompleted();
            }
            RaisePropertyChanged("IsCompleted");
            RaisePropertyChanged("Description");
            
        }

        public override void Disconnect()
        {
            AgentCallBack.DisConnect();
        }

        public override void RejectMessage()
        {
            AgentCallBack.RejectMessage();
        }

        public override bool IsCompleted
        {
            get
            {
                try
                {
                    return AgentCallBack.IsCompleted();
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public override SmtpSessionData SessionData
        {
            get
            {
                return AgentCallBack.GetSessionData();
            }
        }

        public MailItemData MailItemData
        {
            get
            {
                return AgentCallBack.GetMailItemData();
            }
        }

        protected override void GetRecipients()
        {
            _recipients = AgentCallBack.GetRecipients();
        }

        public override void RemoveRecipient(string rcpt)
        {
            AgentCallBack.RemoveRecipient(rcpt);
            _recipients.Clear();
            RaisePropertyChanged("Recipients");
        }
    }

    public class EndOfHeaderScanner : SmtpAgentScannerBase
    {
        protected IAgentSmtpCallback3 AgentCallBack { get; set; }

        public EndOfHeaderScanner(IAgentSmtpCallback3 callback)
        {
            AgentCallBack = callback;
            AgentHookPoint = HookPoint.OnSmtpEndOfHeader;
        }

        public override void RejectMessage()
        {
            AgentCallBack.RejectMessage();
        }

        public override void ScanComplete()
        {
            if (AgentCallBack != null && !AgentCallBack.IsCompleted())
            {
                AgentCallBack.ScanCompleted();
            }
            RaisePropertyChanged("IsCompleted");
            RaisePropertyChanged("Description");
            
        }

        public override bool IsCompleted
        {
            get
            {
                try
                {
                    return AgentCallBack.IsCompleted();
                }
                catch (Exception ex)
                {
                    return false;
                }
                
            }
        }

        public override SmtpSessionData SessionData
        {
            get
            {
                try
                {
                    return AgentCallBack.GetSessionData();
                }
                catch (Exception ex)
                {
                    return null;
                }
                
            }
        }

        public MailItemData MailItemData
        {
            get
            {
                try
                {
                    return AgentCallBack.GetMailItemData();
                }
                catch (Exception ex)
                {
                    return null;
                }
                
            }
        }

        public override void Disconnect()
        {
            AgentCallBack.DisConnect();
        }

        protected override void GetRecipients()
        {
            _recipients = AgentCallBack.GetRecipients();
        }

        public override void RemoveRecipient(string rcpt)
        {
            AgentCallBack.RemoveRecipient(rcpt);
            _recipients.Clear();
            RaisePropertyChanged("Recipients");
        }
    }

    public class SmtpConnectScanner : SmtpAgentScannerBase
    {
        protected IAgentSmtpCallback1 AgentCallBack { get; set; }

        public SmtpConnectScanner(IAgentSmtpCallback1 callback)
        {
            AgentCallBack = callback;
            AgentHookPoint = HookPoint.OnSmtpConnect;
        }

        public override void ScanComplete()
        {
            if (AgentCallBack != null && !AgentCallBack.IsCompleted())
            {
                AgentCallBack.ScanCompleted();
            }
            RaisePropertyChanged("IsCompleted");
            RaisePropertyChanged("Description");
           
        }

        public void RejectConnection()
        {
            AgentCallBack.RejectConnection();
        }

        public override bool IsCompleted
        {
            get
            {
                try
                {
                    return AgentCallBack.IsCompleted();
                }
                catch (Exception ex)
                {
                    return false;
                }
                
            }
        }


        public override void Disconnect()
        {
            AgentCallBack.DisConnect();
        }

        public override SmtpSessionData SessionData
        {
            get
            {
                try
                {
                    return AgentCallBack.GetSessionData();
                }
                catch (Exception ex)
                {
                    return null;
                }
               
            }
        }

        protected override void GetRecipients()
        {
            _recipients = new List<string>();
        }

        public override void RemoveRecipient(string rcpt)
        {
            
        }
    }

    public class SmtpRcptCommandScanner : SmtpAgentScannerBase
    {
        protected IAgentSmtpCallback2 AgentCallBack { get; set; }

        public SmtpRcptCommandScanner(IAgentSmtpCallback2 callback)
        {
            AgentCallBack = callback;
            AgentHookPoint = HookPoint.OnSmtpRcptCommand;
        }

        public override void Disconnect()
        {
            AgentCallBack.DisConnect();
        }

        public void RejectConnection()
        {
            AgentCallBack.RejectConnection();
        }

        public override void ScanComplete()
        {
            if (AgentCallBack != null && !AgentCallBack.IsCompleted())
            {
                AgentCallBack.ScanCompleted();
            }
            RaisePropertyChanged("IsCompleted");
            RaisePropertyChanged("Description");
           
        }

        public override bool IsCompleted
        {
            get
            {
                return AgentCallBack.IsCompleted();
            }
        }

        public override SmtpSessionData SessionData
        {
            get
            {
                try
                {
                    return AgentCallBack.GetSessionData();
                }
                catch (Exception ex)
                {
                    return null;
                }
               
            }
        }

        protected override void GetRecipients()
        {
            _recipients = new List<string>();
        }

        public override void RemoveRecipient(string rcpt)
        {

        }
    }

    public abstract class TansportAgentScannerBase : Scanner
    {
        protected IAgentCallback AgentCallBack { get; set; }
        public TansportAgentScannerBase(IAgentCallback callback)
        {
            AgentCallBack = callback;
        }

        public override void ScanComplete()
        {
            if (AgentCallBack != null && !AgentCallBack.IsCompleted())
            {
                AgentCallBack.ScanCompleted();
            }
            RaisePropertyChanged("IsCompleted");
            RaisePropertyChanged("Description");
        }

        public override bool IsCompleted
        {
            get
            {
                try
                {
                    return AgentCallBack.IsCompleted();
                }
                catch (Exception ex)
                {
                    return false;
                }
                
            }
        }

        public MailItemData MailItemData
        {
            get
            {
                try
                {
                    return AgentCallBack.GetMailItemData();
                }
                catch (Exception ex)
                {
                    return null;
                }
                
            }
        }

        protected override void GetRecipients()
        {
            _recipients = AgentCallBack.GetRecipients();
        }

        public override void RemoveRecipient(string rcpt)
        {
            AgentCallBack.RemoveRecipient(rcpt);
            _recipients.Clear();
            RaisePropertyChanged("Recipients");
        }

        public void Delete()
        {
            AgentCallBack.Delete();
        }
    }

    public class SubmittedMessageScannger : TansportAgentScannerBase
    {
        public SubmittedMessageScannger(IAgentCallback callback)
            :base(callback)
        {
            AgentHookPoint = HookPoint.OnSubmittedMessage;
        }
    }

    public class ResolvedMessageScannger : TansportAgentScannerBase
    {
        public ResolvedMessageScannger(IAgentCallback callback)
            : base(callback)
        {
            AgentHookPoint = HookPoint.OnResolvedMessage;
        }
    }

    public class CategorizedMessageScannger : TansportAgentScannerBase
    {
        public CategorizedMessageScannger(IAgentCallback callback)
            : base(callback)
        {
            AgentHookPoint = HookPoint.OnCategorizedMessage;
        }
    }

    public class RoutedMessageScannger : TansportAgentScannerBase
    {
        public RoutedMessageScannger(IAgentCallback callback)
            : base(callback)
        {
            AgentHookPoint = HookPoint.OnRoutedMessage;
        }
    }

    public class ScannerFactory
    {
        public static Scanner CreateScanner(object callback, AgentCallbackInterfaceTypes callbackType)
        {
            switch (callbackType)
            { 
                case AgentCallbackInterfaceTypes.IAgentCallback:
                    IAgentCallback ac = callback as IAgentCallback;
                    return TansportAgentScannerFactory.CreateScanner((IAgentCallback)callback, ac.GetEventType());
                case AgentCallbackInterfaceTypes.IAgentSmtpCallback1:
                    IAgentSmtpCallback1 ac1 = callback as IAgentSmtpCallback1;
                    return SmtpAgentScannerFactory1.CreateScanner(ac1, ac1.GetEventType());
                case AgentCallbackInterfaceTypes.IAgentSmtpCallback2:
                    IAgentSmtpCallback2 ac2 = callback as IAgentSmtpCallback2;
                    return SmtpAgentScannerFactory2.CreateScanner(ac2, ac2.GetEventType());
                case AgentCallbackInterfaceTypes.IAgentSmtpCallback3:
                    IAgentSmtpCallback3 ac3 = callback as IAgentSmtpCallback3;
                    return SmtpAgentScannerFactory3.CreateScanner(ac3, ac3.GetEventType());
                default:
                    throw new ArgumentNullException("factory donnot know such type");
            }
        }
    }

    public class SmtpAgentScannerFactory1
    {
        public static SmtpAgentScannerBase CreateScanner(IAgentSmtpCallback1 callback, HookPoint hp)
        {
            switch (hp)
            {
                case HookPoint.OnSmtpConnect:
                    return new SmtpConnectScanner(callback);
                default:
                    throw new ArgumentNullException("factory donnot know such hook type");
            }
        }
    }

    public class SmtpAgentScannerFactory2
    {
        public static SmtpAgentScannerBase CreateScanner(IAgentSmtpCallback2 callback, HookPoint hp)
        {
            switch (hp)
            {
                case HookPoint.OnSmtpRcptCommand:
                    return new SmtpRcptCommandScanner(callback);
                default:
                    throw new ArgumentNullException("factory donnot know such hook type");
            }
        }
    }

    public class SmtpAgentScannerFactory3
    {
        public static SmtpAgentScannerBase CreateScanner(IAgentSmtpCallback3 callback, HookPoint hp)
        {
            switch (hp)
            {
                case HookPoint.OnSmtpEndOfData:
                    return new EndOfDataScanner(callback);
                case HookPoint.OnSmtpEndOfHeader:
                    return new EndOfHeaderScanner(callback);
                default:
                    throw new ArgumentNullException("factory donnot know such hook type");
            }
        }
    }

    public class TansportAgentScannerFactory
    {
        public static TansportAgentScannerBase CreateScanner(IAgentCallback callback, HookPoint hp)
        {
            switch (hp)
            { 
                case HookPoint.OnCategorizedMessage:
                    return new CategorizedMessageScannger(callback);
                case HookPoint.OnResolvedMessage:
                    return new ResolvedMessageScannger(callback);
                case HookPoint.OnRoutedMessage:
                    return new RoutedMessageScannger(callback);
                case HookPoint.OnSubmittedMessage:
                    return new SubmittedMessageScannger(callback);
                default:
                    throw new ArgumentNullException("factory donnot know such hook type");
            }
        }
    }

    public class ScannerObservableCollection : ObservableCollection<Scanner>
    { 
          
    }
}
