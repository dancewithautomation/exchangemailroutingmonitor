#$EXDIR="C:\Program Files\Microsoft\Exchange Server\V15\TransportRoles\agents"
Net Stop MSExchangeTransport

Write-Output "Disabling Agent..."
Disable-TransportAgent -Identity "SmtpAgentOnSmtpConnect" -Confirm:$false
Disable-TransportAgent -Identity "SmtpAgentOnSmtpRCPT" -Confirm:$false
Disable-TransportAgent -Identity "SmtpAgentOnEndOfHeaders" -Confirm:$false
Disable-TransportAgent -Identity "SmtpAgentOnSmtpEndOfData" -Confirm:$false
Disable-TransportAgent -Identity "TransportAgentOnRoutedMessage" -Confirm:$false
Disable-TransportAgent -Identity "TransportAgentOnCategorizedMessage" -Confirm:$false
Disable-TransportAgent -Identity "TransportAgentOnResolvedMessage" -Confirm:$false
Disable-TransportAgent -Identity "TransportAgentOnSubmittedMessage" -Confirm:$false

Write-Output "Uninstalling Agent.."
Uninstall-TransportAgent -Identity "SmtpAgentOnSmtpConnect" -Confirm:$false
Uninstall-TransportAgent -Identity "SmtpAgentOnSmtpRCPT" -Confirm:$false
Uninstall-TransportAgent -Identity "SmtpAgentOnEndOfHeaders" -Confirm:$false
Uninstall-TransportAgent -Identity "SmtpAgentOnSmtpEndOfData" -Confirm:$false
Uninstall-TransportAgent -Identity "TransportAgentOnRoutedMessage" -Confirm:$false
Uninstall-TransportAgent -Identity "TransportAgentOnCategorizedMessage" -Confirm:$false
Uninstall-TransportAgent -Identity "TransportAgentOnResolvedMessage" -Confirm:$false
Uninstall-TransportAgent -Identity "TransportAgentOnSubmittedMessage" -Confirm:$false

Net Start MsExchangeTransport

Write-Output "Uninstall Complete."
