$EXDIR="C:\TransportEventMonitor"
Net Stop MSExchangeTransport

Write-Output "Creating directories"
New-Item -Type Directory -path $EXDIR\Log     -ErrorAction SilentlyContinue

Write-Output "Registering agent"

Install-TransportAgent -Name "SmtpAgentOnSmtpConnect" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeSmtpAgentFactory
Install-TransportAgent -Name "SmtpAgentOnSmtpRCPT" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeSmtpAgentFactory2
Install-TransportAgent -Name "SmtpAgentOnSmtpEndOfData" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeSmtpAgentFactory3
Install-TransportAgent -Name "SmtpAgentOnEndOfHeaders" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeSmtpAgentFactory4
Install-TransportAgent -Name "TransportAgentOnRoutedMessage" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeTransportAgentFactory
Install-TransportAgent -Name "TransportAgentOnCategorizedMessage" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeTransportAgentFactory2
Install-TransportAgent -Name "TransportAgentOnResolvedMessage" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeTransportAgentFactory3
Install-TransportAgent -Name "TransportAgentOnSubmittedMessage" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeTransportAgentFactory4

Write-Output "Enabling agent"

Enable-TransportAgent -Identity "SmtpAgentOnSmtpConnect"
Enable-TransportAgent -Identity "SmtpAgentOnSmtpRCPT"
Enable-TransportAgent -Identity "SmtpAgentOnSmtpEndOfData"
Enable-TransportAgent -Identity "SmtpAgentOnEndOfHeaders"
Enable-TransportAgent -Identity "TransportAgentOnRoutedMessage" 
Enable-TransportAgent -Identity "TransportAgentOnCategorizedMessage" 
Enable-TransportAgent -Identity "TransportAgentOnResolvedMessage" 
Enable-TransportAgent -Identity "TransportAgentOnSubmittedMessage" 

Net Start MSExchangeTransport

Write-Output "Install Complete. Please exit the Exchange Management Shell."