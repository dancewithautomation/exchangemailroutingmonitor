$EXDIR="C:\TransportEventMonitor"
Net Stop MSExchangeTransport
Net Stop MSExchangeFrontEndTransport

Write-Output "Creating directories"
New-Item -Type Directory -path $EXDIR\Log     -ErrorAction SilentlyContinue

Write-Output "Registering agent"

Install-TransportAgent -Name "AgentOnSmtpConnect" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeSmtpAgentFactory -TransportService FrontEnd
Install-TransportAgent -Name "AgentOnSmtpRCPT" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeSmtpAgentFactory2 -TransportService FrontEnd
Install-TransportAgent -Name "AgentOnSmtpEndOfData" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeSmtpAgentFactory3 -TransportService FrontEnd
Install-TransportAgent -Name "AgentOnEndOfHeaders" -AssemblyPath $EXDIR\TransportAgents.dll -TransportAgentFactory TransportAgents.ExchangeSmtpAgentFactory4 -TransportService FrontEnd

Write-Output "Enabling agent"
Enable-TransportAgent -Identity "AgentOnSmtpConnect" -TransportService FrontEnd
Enable-TransportAgent -Identity "AgentOnSmtpRCPT" -TransportService FrontEnd
Enable-TransportAgent -Identity "AgentOnSmtpEndOfData" -TransportService FrontEnd
Enable-TransportAgent -Identity "AgentOnEndOfHeaders" -TransportService FrontEnd


Net Start MSExchangeTransport
Net Start MSExchangeFrontEndTransport
Write-Output "Install Complete. Please exit the Exchange Management Shell."
