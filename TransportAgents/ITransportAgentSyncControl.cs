﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport;

namespace TransportAgents
{
    public interface ITransportAgentSyncControl
    {
        AgentAsyncContext GetOwneredAgentAsyncContext();
    }
}
