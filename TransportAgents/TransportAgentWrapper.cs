﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;
using AgentCommon;
using Microsoft.Exchange.Data.Transport;
using System.ServiceModel;
using System.IO;
using Microsoft.Exchange.Data.Transport.Smtp;

namespace TransportAgents
{
    public class TransportAgentWrapper:TransportAgentWrapperBase
    {


        private QueuedMessageEventSource _realSource;
        private QueuedMessageEventArgs _realEventArgs;
        private IScanCallback _realScanEntryProxy=null;

        public TransportAgentWrapper(RoutingAgent agent,HookPoint hp)
            :base(agent,hp)
        {
            try
            {
                if (_realScanEntryProxy == null)
                {
                    DuplexChannelFactory<IScanCallback> pipeFactory =
                    new DuplexChannelFactory<IScanCallback>(new InstanceContext(Agent),
                    new NetNamedPipeBinding(),
                    new EndpointAddress(
                    "net.pipe://localhost/ScanEntry"));
                    _realScanEntryProxy = pipeFactory.CreateChannel();
                    _scanEntryProxy = _realScanEntryProxy;
                    _needFork = _realScanEntryProxy.IsForkedNeed(Hook);
                    _scannerProxyFailed = false;
                }
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
            }
        }

        public override MailItem GetMailItem()
        {
            if (_realEventArgs != null)
            {
                return _realEventArgs.MailItem;
            }
            else
            {
                return null;
            }
        }

        public override void Append2ScanQueue()
        {
            _realScanEntryProxy.Append2ScanQueue(AgentCallbackInterfaceTypes.IAgentCallback);
        }

        public override void UpdateEventSourceAndArgs(object source, object e)
        {
            base.UpdateEventSourceAndArgs(source, e);
            _realEventArgs = e as QueuedMessageEventArgs;
            _realSource = source as QueuedMessageEventSource;
        }

        public override void Update(object source, object e)
        {
            try
            {
                base.Update(source, e);
                if (_needFork)
                {
                    Fork();
                }
                Append2ScanQueue();
            }
            catch (Exception ex)
            {
                ScanCompleted();
                Utilities.Log(ex.ToString());
            }
        }

        public override void NotifyServer(string s)
        {
            try
            {
                _realScanEntryProxy.Log(s);
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
            }
        }

        //public void Update(QueuedMessageEventSource source, QueuedMessageEventArgs e)
        //{
        //    try
        //    {
        //        _eventFiredTimes++;
        //        _isScanCompleted = false;
        //        NotifyServer(Hook.ToString()+" Event Triggered,Times=" + _eventFiredTimes.ToString());
        //        _source = source;
        //        _mailItem  = e.MailItem;
        //        _mailItemData = GetMailItemData();
        //        _recipients = GetRecipients();
        //        _agentAsyncContext = (Agent as ISmexTransportAgentSyncControl).GetOwneredAgentAsyncContext();
        //        if (_needFork)
        //        {
        //            Fork();
        //        }
        //        _scanEntryProxy.Append2ScanQueue(AgentCallbackInterfaceTypes.IAgentCallback);
        //    }
        //    catch (Exception ex)
        //    {
        //        ScanCompleted();
        //        Utilities.Log(ex.ToString());
        //    }
        //}

        //public List<string> GetRecipients()
        //{
        //    try
        //    {
        //        if (_isScanCompleted)
        //        {
        //            return _recipients;
        //        }
        //        else
        //        { 
        //            return Utilities.GetRecipients(_mailItem);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return _recipients;
        //    }

        //}

        //public void RemoveRecipient(string rcpt)
        //{
        //    Utilities.RemoveRecipient(_mailItem, rcpt);
        //}

        public override void Fork()
        {
            try
            {
                if (_isScanCompleted)
                {
                    return;
                }

                if (_mailItem.Recipients.Count > 1)
                {
                    NotifyServer("Recipient Count is >1, forking...");
                    int count = 0;
                    while (_mailItem.Recipients.Count > 1)
                    {
                        // Create an individual forked message for each recipient...
                        EnvelopeRecipient recip = _mailItem.Recipients[_mailItem.Recipients.Count - 1];
                        NotifyServer("forking " + recip.Address.ToString() + "...");
                        List<EnvelopeRecipient> recips = new List<EnvelopeRecipient>();
                        recips.Add(recip);
                        _realSource.Fork(recips);
                        count++;
                    }
                }
                else
                {
                    NotifyServer("Recipient Count is 1, no need to fork message");
                }
            }
            catch (Exception ex)
            {
                NotifyServer("Error in Fork:" + ex.Message);
            }
        }


        public override void Delete()
        {
           
            Utilities.Log("Dleting...");
            try
            {
                _realSource.Delete();
                Utilities.Log("Deleted");
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
                NotifyServer(ex.Message);
            }
        }
       
    }
}
