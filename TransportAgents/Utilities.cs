﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Exchange.Data.Transport;
using Microsoft.Exchange.Data.Mime;
using AgentCommon;
using Microsoft.Exchange.Data.Transport.Smtp;

namespace TransportAgents
{
    internal static class Utilities
    {
        private static SmtpResponse _rejectResponse =
                new SmtpResponse("250", "", "rejected event");
        public static void GetMimeContent(Stream messageStream, out string content)
        {
            const int bufferSize = 4000;
            byte[] buffer = new byte[bufferSize];
            //messageStream.Position = 0;
            StringBuilder sb = new StringBuilder();
            while (messageStream.Position < messageStream.Length)
            {
                int bytesRead = messageStream.Read(buffer, 0, bufferSize);
                Decoder decoder = Encoding.Default.GetDecoder();
                char[] chars = new char[decoder.GetCharCount(buffer, 0, bufferSize)];
                decoder.GetChars(buffer, 0, bufferSize, chars, 0);
                sb.Append(chars);
            }
            content = sb.ToString();
        }

        public static void GetMimeContent(Stream messageStream, out string content,string charset)
        {
            const int bufferSize = 4000;
            byte[] buffer = new byte[bufferSize];
            //messageStream.Position = 0;
            //System.Net.Mime.ContentType c = new System.Net.Mime.ContentType(contentType);
            StreamReader reader = new StreamReader(messageStream,Encoding.GetEncoding(charset));
            string Body = reader.ReadToEnd(); 
            //StringBuilder sb = new StringBuilder();
            //while (messageStream.Position < messageStream.Length)
            //{
            //    int bytesRead = messageStream.Read(buffer, 0, bufferSize);
            //    System.Net.Mime.ContentType c = new System.Net.Mime.ContentType(contentType);
                
            //    //Decoder decoder = System.Net.Mime.ContentType
            //    char[] chars = new char[decoder.GetCharCount(buffer, 0, bufferSize)];
            //    decoder.GetChars(buffer, 0, bufferSize, chars, 0);
            //    sb.Append(chars);
            //}
            content = Body;
        }

        public static void AddHeader(MailItem item, string name,string value)
        {
            //item.Message.Subject = item.Message.Subject + "| To: " + item.Recipients[0].Address.LocalPart;

            MimeDocument mdMimeDoc = item.Message.MimeDocument;
            HeaderList hlHeaderlist = mdMimeDoc.RootPart.Headers;
            MimeNode lhLasterHeader = hlHeaderlist.LastChild;
            
            // Add a custom header
            TextHeader nhNewHeader = new TextHeader(name, value);
            hlHeaderlist.InsertBefore(nhNewHeader, lhLasterHeader);
        }

        public static  void SetMimeStream(Stream orginalstream,Stream newstream)
        {
            try
            {
                using (Stream writeStream = orginalstream)
                {
                    int bytesRead = 0;
                    byte[] buffer = new byte[4000];
                    while ((bytesRead = newstream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        writeStream.Write(buffer, 0, bytesRead);
                    }
                    writeStream.Flush();
                }
            }
            finally
            {
                newstream.Close();
            }
        }

        public static void Log(string s)
        {
            try
            {
                string file = @"c:\agentLog.txt";
                File.AppendAllText(file, DateTime.Now.ToString()+","+s+Environment.NewLine);
            }
            catch (Exception ex)
            { }
        }

        public static SmtpSessionData GetSessionData(SmtpSession _session)
        {
            SmtpSessionData data = new SmtpSessionData();

            if (_session == null)
            {
                return data;
            }
            data.IsConnected = _session.IsConnected;
            data.IsExternalConnection = _session.IsExternalConnection;
            data.SessionId = _session.SessionId;
            data.AntispamBypass = _session.AntispamBypass;
            data.IsTls = _session.IsTls;
            data.AuthenticationSource = _session.AuthenticationSource.ToString();

            if (_session.HelloDomain != null)
            {
                data.HelloDomain = _session.HelloDomain;
            }
            else
            {
                data.HelloDomain = "NULL";
            }

            if (_session.Properties != null)
            {
                StringBuilder sb = new StringBuilder();
                foreach (KeyValuePair<string, object> kv in _session.Properties)
                {
                    if (kv.Value != null)
                    {
                        sb.AppendLine(kv.Key + ":" + kv.Value.ToString());
                    }
                    else
                    {
                        sb.AppendLine(kv.Key + ":NULL");
                    }
                }
                data.Properties = sb.ToString();
            }
            else
            {
                data.Properties = "NULL";
            }

            if (_session.LocalEndPoint != null)
            {
                data.LocalEndPoint = _session.LocalEndPoint.ToString();
            }
            else
            {
                data.LocalEndPoint = "NULL";
            }
            if (_session.RemoteEndPoint != null)
            {
                data.RemoteEndPoint = _session.RemoteEndPoint.ToString();
            }
            else
            {
                data.RemoteEndPoint = "NULL";
            }
            if (_session.LastExternalIPAddress != null)
            {
                data.LastExternalIp = _session.LastExternalIPAddress.ToString();
            }
            else
            {
                data.LastExternalIp = "NULL";
            }
            return data;
        }

        public static MailItemData GetMailItemData(MailItem _mail)
        {
            MailItemData data = new MailItemData();
            string body=null;
            data.FromAddress = _mail.FromAddress.ToString();
            data.Recipients = new List<string>();
            if (_mail.Recipients != null)
            {
                foreach (var v in _mail.Recipients)
                {
                    data.Recipients.Add(v.Address.ToString());
                }
            }
            if (_mail.OriginalAuthenticator != null)
            {
                data.OriginalAuthenticator = _mail.OriginalAuthenticator;
            }
            else
            {
                data.OriginalAuthenticator = "NULL";
            }
            if (_mail.OriginatingDomain != null)
            {
                data.OriginatingDomain = _mail.OriginatingDomain;
            }
            else
            {
                data.OriginatingDomain = "NULL";
            }
            if (_mail.OriginatorOrganization != null)
            {
                data.OriginatorOrganization = _mail.OriginatorOrganization;
            }
            else
            {
                data.OriginatorOrganization = "NULL";
            }
            data.DateTimeReceived = _mail.DateTimeReceived.ToString();
            data.DeliveryPriority = _mail.DeliveryPriority.ToString();
            data.DsnFormatRequested = _mail.DsnFormatRequested.ToString();
            data.InboundDeliveryMethod = _mail.InboundDeliveryMethod.ToString();
            data.MimeStreamLength = _mail.MimeStreamLength;
            data.MustDeliver=_mail.MustDeliver;
            if (_mail.EnvelopeId != null)
            {
                data.EnvelopeId = _mail.EnvelopeId;
            }
            else
            {
                data.EnvelopeId = "NULL";
            }
            if (_mail.TenantId != null)
            {
                data.TenantId = _mail.TenantId.ToString();
            }
            else
            {
                data.TenantId = "NULL";
            }
            if (_mail.Properties != null)
            {
                StringBuilder sb = new StringBuilder();
                foreach (KeyValuePair<string, object> kv in _mail.Properties)
                {
                    if (kv.Value != null)
                    {
                        sb.AppendLine(kv.Key + ":" + kv.Value.ToString());
                    }
                    else
                    {
                        sb.AppendLine(kv.Key + ":NULL");
                    }
                }
                data.Properties = sb.ToString();
            }

            if (_mail.Message == null)
            {
                return data;
            }

            data.ContentType = _mail.Message.RootPart.ContentType;
            
            //_mail.Message.MimeDocument.HeaderDecodingOptions.CharsetEncoding.
            if (_mail.Message != null && _mail.Message.Body != null)
            {
                data.Charset = _mail.Message.Body.CharsetName;
                if (_mail.Message.MimeDocument.HeaderDecodingOptions.CharsetEncoding == null)
                {
                    data.HeaderDecodingCodeName = "null";
                }
                else
                {
                    data.HeaderDecodingCodeName = _mail.Message.MimeDocument.HeaderDecodingOptions.CharsetEncoding.EncodingName;
                }
                
                //_mail.Message.RootPart.
                Utilities.GetMimeContent(_mail.Message.Body.GetContentReadStream(), out body,data.Charset);
                data.Subject = _mail.Message.Subject;
                data.MessageId = _mail.Message.MessageId;
                data.Body = body;
                data.Headers = new List<string>();
                foreach (var v in _mail.Message.MimeDocument.RootPart.Headers)
                {
                    data.Headers.Add(v.Name + ":" + v.Value);
                }
                if (_mail.Message.From != null)
                {
                    data.From = _mail.Message.From.SmtpAddress.ToString();
                }
                else
                {
                    data.From = "NULL";
                }
                data.IsInterpersonalMessage=_mail.Message.IsInterpersonalMessage;
                data.IsOpaqueMessage = _mail.Message.IsOpaqueMessage;
                data.IsSystemMessage = _mail.Message.IsSystemMessage;
                data.MapiMessageClass = _mail.Message.MapiMessageClass;
                data.MessageSecurityType = _mail.Message.MessageSecurityType.ToString();
                if (_mail.Message.Sender != null)
                {
                    data.Sender = _mail.Message.Sender.SmtpAddress.ToString();
                }
                else
                {
                    data.Sender = "NULL";
                }
                if (_mail.Message.To != null)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (Microsoft.Exchange.Data.Transport.Email.EmailRecipient er in _mail.Message.To)
                    {
                        sb.AppendLine(er.SmtpAddress.ToString());
                    }
                    data.To = sb.ToString();
                }
                else
                {
                    data.To = "NULL";
                }
            }
            
            return data;
        }

        public static void RemoveRecipient(MailItem _mail,string rcpt)
        {
            foreach (EnvelopeRecipient recipient in _mail.Recipients)
            {
                if (rcpt.Contains(recipient.Address.LocalPart))
                {
                    _mail.Recipients.Remove(recipient, DsnType.Failure, _rejectResponse);
                }
            }
        }

        public static List<string> GetRecipients(MailItem _mail)
        {
            List<string> c = new List<string>();
            
            foreach (EnvelopeRecipient recipient in _mail.Recipients)
            {
                c.Add(recipient.Address.ToString());
            }
            return c;
        }
    }
}
