﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Exchange.Data.Transport.Smtp;
using System.Threading;
using AgentCommon;
using Microsoft.Exchange.Data.Transport;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Runtime.Serialization;

namespace TransportAgents
{
    [CallbackBehavior(
        ConcurrencyMode = ConcurrencyMode.Single, UseSynchronizationContext = false)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true,ConcurrencyMode=ConcurrencyMode.Multiple )]
    public class ExchangeSmtpAgent : SmtpReceiveAgent,IAgentSmtpCallback1,ITransportAgentSyncControl
    {
        private SmtpConnectAgentWrapper _wrapper;
        public ExchangeSmtpAgent()
        {
            _wrapper = new SmtpConnectAgentWrapper(this, HookPoint.OnSmtpConnect);
            if (!_wrapper.ScannerProxyFailed)
            { 
                this.OnConnect += new ConnectEventHandler(SmexExchangeSmtpAgent_OnConnect);
            }
        }


        void SmexExchangeSmtpAgent_OnConnect(ConnectEventSource source, ConnectEventArgs e)
        {
            try
            {
                _wrapper.Update(source, e);
            }
            catch (Exception ex)
            {
                _wrapper.ScanCompleted();
                Utilities.Log(ex.ToString());
            }
        }

        public void ScanCompleted()
        {
            _wrapper.ScanCompleted();
        }

        public bool IsCompleted()
        {
            return _wrapper.IsCompleted();
        }

        public string GetEventName()
        {
            return _wrapper.GetEventName();
        }

        public void DisConnect()
        {
            _wrapper.DisConnect();
        }

        public bool IsConnected()
        {
            return _wrapper.IsConnected();
        }

        public bool IsExternalConnection()
        {
            return _wrapper.IsExternalConnection();
        }

        public string GetLastExternalIp()
        {
            return _wrapper.GetLastExternalIp();
        }

        public void RejectConnection()
        {
            _wrapper.RejectConnection();
        }

        public HookPoint GetEventType()
        {
            return _wrapper.Hook;
        }


        public SmtpSessionData GetSessionData()
        {
            return _wrapper.GetSessionData();
        }

        public AgentAsyncContext GetOwneredAgentAsyncContext()
        {
            return this.GetAgentAsyncContext();
        }
    }
}
