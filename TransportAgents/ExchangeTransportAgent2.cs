﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Exchange.Data.Transport.Smtp;
using System.Threading;
using AgentCommon;
using Microsoft.Exchange.Data.Transport;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Runtime.Serialization;

namespace TransportAgents
{
    [CallbackBehavior(
        ConcurrencyMode = ConcurrencyMode.Single, UseSynchronizationContext = false)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true,ConcurrencyMode=ConcurrencyMode.Multiple )]
    public class ExchangeTransportAgent2 : RoutingAgent,IAgentCallback,ITransportAgentSyncControl
    {
        private TransportAgentWrapper _wrapper;
        public ExchangeTransportAgent2()
        {
            _wrapper = new TransportAgentWrapper(this, HookPoint.OnCategorizedMessage);
            if (!_wrapper.ScannerProxyFailed)
            {
                this.OnCategorizedMessage += new CategorizedMessageEventHandler(SmexExchangeTransportAgent2_OnCategorizedMessage);
            }  
        }

        void SmexExchangeTransportAgent2_OnCategorizedMessage(CategorizedMessageEventSource source, QueuedMessageEventArgs e)
        {
            try
            {
                _wrapper.Update(source, e);
            }
            catch (Exception ex)
            {
                _wrapper.ScanCompleted();
                Utilities.Log(ex.ToString());
            } 
        }

        public void GetMimeContent(out string content)
        {
            _wrapper.GetMimeContent(out content);
        }

        public void ScanCompleted()
        {
            _wrapper.ScanCompleted();
        }

        public bool IsCompleted()
        {
            return _wrapper.IsCompleted(); ;
        }

        public void Delete()
        {
            _wrapper.Delete();
        }

        public string GetSubject()
        {
            return _wrapper.GetSubject();
        }

        public string GetEventName()
        {
            return _wrapper.GetEventName();
        }

        public void AddHeader(string name, string value)
        {
            _wrapper.AddHeader(name, value);
        }

        public void Fork()
        {
            _wrapper.Fork();
        }

        public HookPoint GetEventType()
        {
            return _wrapper.Hook;
        }

        public MailItemData GetMailItemData()
        {
            return _wrapper.GetMailItemData();
        }

        public List<string> GetRecipients()
        {
            return _wrapper.GetRecipients();
        }

        public void RemoveRecipient(string rcpt)
        {
            _wrapper.RemoveRecipient(rcpt);
        }

        public AgentAsyncContext GetOwneredAgentAsyncContext()
        {
            return this.GetAgentAsyncContext();
        }
    }
}
