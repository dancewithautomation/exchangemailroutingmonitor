﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;
using AgentCommon;
using Microsoft.Exchange.Data.Transport;
using System.ServiceModel;
using System.IO;
using Microsoft.Exchange.Data.Transport.Smtp;

namespace TransportAgents
{
    public class SmtpRCPTAgentWrapper:TransportAgentWrapperBase
    {


        private ReceiveCommandEventSource _realSource;
        private RcptCommandEventArgs _realEventArgs;
        private IScanCallback3 _realScanEntryProxy = null;

        public SmtpRCPTAgentWrapper(SmtpReceiveAgent agent, HookPoint hp)
            :base(agent,hp)
        {
            try
            {
                if (_realScanEntryProxy == null)
                {
                    DuplexChannelFactory<IScanCallback3> pipeFactory =
                    new DuplexChannelFactory<IScanCallback3>(new InstanceContext(Agent),
                    new NetNamedPipeBinding(),
                    new EndpointAddress(
                    "net.pipe://localhost/ScanEntry3"));
                    _realScanEntryProxy = pipeFactory.CreateChannel();
                    _scanEntryProxy = _realScanEntryProxy;
                    _scannerProxyFailed = false;
                }
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
            }
        }

        public override void DisConnect()
        {
            Utilities.Log("disconnecting...");
            try
            {
                _realSource.Disconnect();
                Utilities.Log("disconnected");
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
                NotifyServer(ex.Message);
            }
        }

        public override void RejectConnection()
        {
            Utilities.Log("rejecting connection...");
            try
            {
                _realSource.RejectCommand(_rejectResponse);
                Utilities.Log("rejected");
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
                NotifyServer(ex.Message);
            }
        }

        public override void Append2ScanQueue()
        {
            _realScanEntryProxy.Append2ScanQueue(AgentCallbackInterfaceTypes.IAgentSmtpCallback2);
        }

        public override SmtpSession GetSmtpSession()
        {
            return _realEventArgs.SmtpSession;
        }

        public override void UpdateEventSourceAndArgs(object source, object e)
        {
            base.UpdateEventSourceAndArgs(source, e);
            _realEventArgs = e as RcptCommandEventArgs;
            _realSource = source as ReceiveCommandEventSource;
        }

        public override string GetOtherInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("{");
        
            sb.AppendLine("Notify:" + _realEventArgs.Notify.ToString());
     
            if (_realEventArgs.OriginalRecipient != null)
            {
                sb.AppendLine("OriginalRecipient:" + _realEventArgs.OriginalRecipient.ToString());
            }
            if (_realEventArgs.RecipientAddress != null)
            {
                sb.AppendLine("RecipientAddress:" + _realEventArgs.RecipientAddress.ToString());
            }
            sb.AppendLine("}");
            return sb.ToString();
        }

        public override void Delete()
        {
            base.Delete();
        }
        public override void Update(object source, object e)
        {
            try
            {
                base.Update(source, e);
                Append2ScanQueue();
            }
            catch (Exception ex)
            {
                ScanCompleted();
                Utilities.Log(ex.ToString());
            }
        }

        public override void NotifyServer(string s)
        {
            try
            {
                _realScanEntryProxy.Log( s);
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
            }
        }

        public override void RejectRecipient()
        {
            RejectConnection();
        }
        //public void Update(QueuedMessageEventSource source, QueuedMessageEventArgs e)
        //{
        //    try
        //    {
        //        _eventFiredTimes++;
        //        _isScanCompleted = false;
        //        NotifyServer(Hook.ToString()+" Event Triggered,Times=" + _eventFiredTimes.ToString());
        //        _source = source;
        //        _mailItem  = e.MailItem;
        //        _mailItemData = GetMailItemData();
        //        _recipients = GetRecipients();
        //        _agentAsyncContext = (Agent as ISmexTransportAgentSyncControl).GetOwneredAgentAsyncContext();
        //        if (_needFork)
        //        {
        //            Fork();
        //        }
        //        _scanEntryProxy.Append2ScanQueue(AgentCallbackInterfaceTypes.IAgentCallback);
        //    }
        //    catch (Exception ex)
        //    {
        //        ScanCompleted();
        //        Utilities.Log(ex.ToString());
        //    }
        //}

        //public List<string> GetRecipients()
        //{
        //    try
        //    {
        //        if (_isScanCompleted)
        //        {
        //            return _recipients;
        //        }
        //        else
        //        { 
        //            return Utilities.GetRecipients(_mailItem);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return _recipients;
        //    }

        //}

        //public void RemoveRecipient(string rcpt)
        //{
        //    Utilities.RemoveRecipient(_mailItem, rcpt);
        //}

        
       
    }
}
