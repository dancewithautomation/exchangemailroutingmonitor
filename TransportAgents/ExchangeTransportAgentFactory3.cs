﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;

namespace TransportAgents
{
    public class ExchangeTransportAgentFactory3 : RoutingAgentFactory
    {
        public override RoutingAgent CreateAgent(Microsoft.Exchange.Data.Transport.SmtpServer server)
        {
            return new ExchangeTransportAgent3();
        }
    }
}
