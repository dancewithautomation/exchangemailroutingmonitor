﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Exchange.Data.Transport.Smtp;
using System.Threading;
using AgentCommon;
using Microsoft.Exchange.Data.Transport;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Runtime.Serialization;

namespace TransportAgents
{
    [CallbackBehavior(
        ConcurrencyMode = ConcurrencyMode.Single, UseSynchronizationContext = false)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ExchangeSmtpAgent2 : SmtpReceiveAgent, IAgentSmtpCallback2, ITransportAgentSyncControl
    {
        private SmtpRCPTAgentWrapper _wrapper;
        
        public ExchangeSmtpAgent2()
        {
            try
            {
                _wrapper = new SmtpRCPTAgentWrapper(this, HookPoint.OnSmtpRcptCommand);
                if (!_wrapper.ScannerProxyFailed)
                {
                    this.OnRcptCommand += new RcptCommandEventHandler(SmexExchangeSmtpAgent2_OnRcptCommand);
                }  
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
            }

        }

        void SmexExchangeSmtpAgent2_OnRcptCommand(ReceiveCommandEventSource source, RcptCommandEventArgs e)
        {
            try
            {
                _wrapper.Update(source, e);
            }
            catch (Exception ex)
            {
                _wrapper.ScanCompleted();
                Utilities.Log(ex.ToString());
            }

        }

        public void ScanCompleted()
        {
            _wrapper.ScanCompleted();
        }

        public bool IsCompleted()
        {
            return _wrapper.IsCompleted();
        }

        public string GetEventName()
        {
            return _wrapper.GetEventName();
        }

        public void DisConnect()
        {
            _wrapper.DisConnect();
        }

        public bool IsConnected()
        {
            return _wrapper.IsConnected();
        }

        public bool IsExternalConnection()
        {
            return _wrapper.IsExternalConnection();
        }

        public string GetLastExternalIp()
        {
            return _wrapper.GetLastExternalIp();
        }

        public void RejectRecipient()
        {
            _wrapper.RejectRecipient();
        }

        public void RejectConnection()
        {
            _wrapper.RejectConnection();
        }

        public void GetMimeContent(out string content)
        {
            _wrapper.GetMimeContent(out content);
        }

        public string GetSubject()
        {
            return _wrapper.GetSubject();
        }

        public HookPoint GetEventType()
        {
            return _wrapper.Hook;
        }

        public SmtpSessionData GetSessionData()
        {
            return _wrapper.GetSessionData();
        }

        public AgentAsyncContext GetOwneredAgentAsyncContext()
        {
            return this.GetAgentAsyncContext();
        }
    }
}
