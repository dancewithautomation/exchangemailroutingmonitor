﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport;
using AgentCommon;
using System.IO;
using Microsoft.Exchange.Data.Transport.Smtp;

namespace TransportAgents
{
    public abstract class TransportAgentWrapperBase
    {
        protected bool _scannerProxyFailed = true;
        public bool ScannerProxyFailed
        {
            get { return _scannerProxyFailed; }
            protected set { _scannerProxyFailed = value; }
        }

        protected HookPoint _hook;
        public HookPoint Hook
        {
            get { return _hook; }
            protected set { _hook = value; }
        }

        protected int _eventFiredTimes = 0;
        protected MailItem _mailItem;
        protected AgentAsyncContext _agentAsyncContext;
        protected object _scanEntryProxy;
        protected bool _isScanCompleted = false;
        protected MailItemData _mailItemData = null;
        protected List<string> _recipients=new List<string>();
        protected object _eventSource;
        protected object _eventArgs;
        protected Agent _agent;
        protected bool _needFork = false;

        public Agent Agent
        {
            get { return _agent; }
            protected set { _agent = value; }
        }

        protected SmtpSession _session;
        protected static SmtpResponse _rejectResponse = new SmtpResponse("550", "5.7.1", "recipient or message rejected by TransportEventMonitor");
        protected SmtpSessionData _sessionData;


        public TransportAgentWrapperBase(Agent agent, HookPoint hp)
        {
            Agent = agent;
            Hook = hp;
        }

        public virtual List<string> GetRecipients()
        {
            try
            {
                if (_isScanCompleted || _mailItem==null)
                {
                    return _recipients;
                }
                else
                {
                    return Utilities.GetRecipients(_mailItem);
                }
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
                return _recipients;
            }

        }

        public virtual void RemoveRecipient(string rcpt)
        {
            try
            {
                if (_mailItem != null)
                {
                    Utilities.Log("removing recipient...");
                    Utilities.RemoveRecipient(_mailItem, rcpt);
                    Utilities.Log("removed");
                }
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
                ScanCompleted();
            }
            
        }

        public virtual void Update(object source, object e)
        {
            try
            {
                _eventFiredTimes++;
                _isScanCompleted = false;
                NotifyServer(Hook.ToString() + " Event Triggered,Times=" + _eventFiredTimes.ToString());
                UpdateEventSourceAndArgs(source, e);
                _mailItem = GetMailItem();
                _mailItemData = GetMailItemData();
                _recipients = GetRecipients();
                _session = GetSmtpSession();
                _sessionData = GetSessionData();
                _agentAsyncContext = (Agent as ITransportAgentSyncControl).GetOwneredAgentAsyncContext();
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
                ScanCompleted();
            }
        }

        public virtual void UpdateEventSourceAndArgs(object source, object e)
        {
            _eventSource = source;
            _eventArgs = e;
        }
        
        public virtual SmtpSession GetSmtpSession()
        {
            return null;
        }

        public virtual String GetOtherInfo()
        {
            return string.Empty;
        }

        public virtual void Fork()
        {
            return;
        }

        public virtual void Delete()
        {
            return;
        }

        public virtual void DisConnect()
        {
            return;
        }

        public bool IsConnected()
        {
            if (_session != null)
            {
                return _session.IsConnected;
            }
            else
            {
                return false;   
            } 
        }

        public bool IsExternalConnection()
        {
            if (_session != null)
            {
                return _session.IsExternalConnection;
            }
            else
            {
                return false;
            }
        }

        public string GetLastExternalIp()
        {
            if (_session != null && _session.LastExternalIPAddress!=null)
            {
                return _session.LastExternalIPAddress.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public virtual void RejectMessage()
        {
            return;
        }

        public virtual void RejectRecipient()
        {
            return;
        }

        public virtual void RejectConnection()
        {
            return;
        }

        public SmtpSessionData GetSessionData()
        {
            try
            {
                if (_session != null)
                {
                    SmtpSessionData data= Utilities.GetSessionData(_session);
                    data.OtherInfo = GetOtherInfo();
                    return data;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                
                NotifyServer(ex.ToString());
                return _sessionData;
            }

        }

        public virtual MailItem GetMailItem()
        {
            return null;
        }

        public abstract void Append2ScanQueue();

        public void ScanCompleted()
        {
            if (!_isScanCompleted)
            {
                if (_agentAsyncContext != null)
                {
                    _agentAsyncContext.Complete();
                }
                _isScanCompleted = true;
            }
        }

        public string GetEventName()
        {
            return Hook.ToString();
        }

        public MailItemData GetMailItemData()
        {
            try
            {
                if (_mailItem == null)
                {
                    return null;
                }

                if (_isScanCompleted)
                {
                    return _mailItemData;
                }
                else
                {
                    return Utilities.GetMailItemData(_mailItem);
                }
            }
            catch(Exception ex)
            {
                NotifyServer(ex.ToString());
                return _mailItemData;
            }
        }


        public void GetMimeContent(out string content)
        {
            if (_mailItem != null)
            {
                Stream messageStream = _mailItem.GetMimeReadStream();
                Utilities.GetMimeContent(messageStream, out content);
            }
            else
            {
                content = string.Empty;
            }
        }

        public bool IsCompleted()
        {
            return _isScanCompleted;
        }


        public string GetSubject()
        {
            if (_mailItem != null)
            {
                return _mailItem.Message.Subject;
            }
            else
            {
                return string.Empty;
            }
            
        }

        public abstract void NotifyServer(string s);
        
        public void AddHeader(string name, string value)
        {
            try
            {
                if (_mailItem == null)
                {
                    return;
                }
                Utilities.AddHeader(_mailItem, name, value);
            }
            catch (Exception ex)
            {
                NotifyServer("Error in AddHeader:" + ex.Message);
            }
        }
    }
}
