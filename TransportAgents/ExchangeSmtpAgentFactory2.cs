﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;
using Microsoft.Exchange.Data.Transport.Smtp;

namespace TransportAgents
{
    public class ExchangeSmtpAgentFactory2 : SmtpReceiveAgentFactory
    {
        public override SmtpReceiveAgent CreateAgent(Microsoft.Exchange.Data.Transport.SmtpServer server)
        {
            return new ExchangeSmtpAgent2();
        }

        
    }
}
