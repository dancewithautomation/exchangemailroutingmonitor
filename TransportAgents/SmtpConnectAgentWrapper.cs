﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;
using AgentCommon;
using Microsoft.Exchange.Data.Transport;
using System.ServiceModel;
using System.IO;
using Microsoft.Exchange.Data.Transport.Smtp;

namespace TransportAgents
{
    public class SmtpConnectAgentWrapper:TransportAgentWrapperBase
    {


        private ConnectEventSource _realSource;
        private ConnectEventArgs _realEventArgs;
        private IScanCallback2 _realScanEntryProxy = null;

        public SmtpConnectAgentWrapper(SmtpReceiveAgent agent, HookPoint hp)
            :base(agent,hp)
        {
            try
            {
                if (_realScanEntryProxy == null)
                {
                    DuplexChannelFactory<IScanCallback2> pipeFactory =
                    new DuplexChannelFactory<IScanCallback2>(new InstanceContext(Agent),
                    new NetNamedPipeBinding(),
                    new EndpointAddress(
                    "net.pipe://localhost/ScanEntry2"));
                    _realScanEntryProxy = pipeFactory.CreateChannel();
                    _scanEntryProxy = _realScanEntryProxy;
                    _scannerProxyFailed = false;
                }
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
            }
        }

        public override void DisConnect()
        {
            
            Utilities.Log("disconnected...");
            try
            {
                _realSource.Disconnect();
                Utilities.Log("disconnected");
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
                NotifyServer(ex.Message);
            }
        }

        public override string GetOtherInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("{");
            sb.AppendLine("DisableStartTls:" + _realEventArgs.DisableStartTls.ToString());
            sb.AppendLine("DsnExplanation:" + _realEventArgs.Banner.DsnExplanation);
            sb.AppendLine("EnhancedStatusCode:" + _realEventArgs.Banner.EnhancedStatusCode);
            sb.AppendLine("StatusCode:" + _realEventArgs.Banner.StatusCode);
            if (_realEventArgs.Banner.StatusText != null)
            {
                foreach (string s in _realEventArgs.Banner.StatusText)
                {
                    sb.AppendLine("StatusText:" + s);
                }
            }
            sb.AppendLine("SmtpResponseType:" + _realEventArgs.Banner.SmtpResponseType.ToString());
            sb.AppendLine("}");
            return sb.ToString();
        }

        public override void RejectConnection()
        {
            
            Utilities.Log("rejecting connection...");
            try
            {
                _realSource.RejectConnection(_rejectResponse);
                Utilities.Log("rejected");
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
                NotifyServer(ex.Message);
            }
        }

        public override void Append2ScanQueue()
        {
            _realScanEntryProxy.Append2ScanQueue(AgentCallbackInterfaceTypes.IAgentSmtpCallback1);
        }

        public override SmtpSession GetSmtpSession()
        {
            return _realEventArgs.SmtpSession;
        }

        public override void UpdateEventSourceAndArgs(object source, object e)
        {
            base.UpdateEventSourceAndArgs(source, e);
            _realEventArgs = e as ConnectEventArgs;
            _realSource = source as ConnectEventSource;
        }

        public override void Update(object source, object e)
        {
            try
            {
                base.Update(source, e);
                Append2ScanQueue();
            }
            catch (Exception ex)
            {
                ScanCompleted();
                Utilities.Log(ex.ToString());
            }
        }

        public override void NotifyServer(string s)
        {
            try
            {
                _realScanEntryProxy.Log(s);
            }
            catch (Exception ex)
            {
                Utilities.Log(ex.ToString());
            }
        }

        //public void Update(QueuedMessageEventSource source, QueuedMessageEventArgs e)
        //{
        //    try
        //    {
        //        _eventFiredTimes++;
        //        _isScanCompleted = false;
        //        NotifyServer(Hook.ToString()+" Event Triggered,Times=" + _eventFiredTimes.ToString());
        //        _source = source;
        //        _mailItem  = e.MailItem;
        //        _mailItemData = GetMailItemData();
        //        _recipients = GetRecipients();
        //        _agentAsyncContext = (Agent as ISmexTransportAgentSyncControl).GetOwneredAgentAsyncContext();
        //        if (_needFork)
        //        {
        //            Fork();
        //        }
        //        _scanEntryProxy.Append2ScanQueue(AgentCallbackInterfaceTypes.IAgentCallback);
        //    }
        //    catch (Exception ex)
        //    {
        //        ScanCompleted();
        //        Utilities.Log(ex.ToString());
        //    }
        //}

        //public List<string> GetRecipients()
        //{
        //    try
        //    {
        //        if (_isScanCompleted)
        //        {
        //            return _recipients;
        //        }
        //        else
        //        { 
        //            return Utilities.GetRecipients(_mailItem);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return _recipients;
        //    }

        //}

        //public void RemoveRecipient(string rcpt)
        //{
        //    Utilities.RemoveRecipient(_mailItem, rcpt);
        //}

        
       
    }
}
