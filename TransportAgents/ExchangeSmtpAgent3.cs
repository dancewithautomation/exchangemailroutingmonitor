﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Exchange.Data.Transport.Routing;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Exchange.Data.Transport.Smtp;
using System.Threading;
using AgentCommon;
using Microsoft.Exchange.Data.Transport;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Runtime.Serialization;

namespace TransportAgents
{
    [CallbackBehavior(
        ConcurrencyMode = ConcurrencyMode.Single, UseSynchronizationContext = false)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true,ConcurrencyMode=ConcurrencyMode.Multiple )]
    public class ExchangeSmtpAgent3 : SmtpReceiveAgent, IAgentSmtpCallback3, ITransportAgentSyncControl
    {
        private SmtpEndAgentWrapper _wrapper;

        public ExchangeSmtpAgent3()
        {
            _wrapper = new SmtpEndAgentWrapper(this, HookPoint.OnSmtpEndOfData);
            if (!_wrapper.ScannerProxyFailed)
            { 
                 this.OnEndOfData += new EndOfDataEventHandler(SmexExchangeSmtpAgent3_OnEndOfData);
                
            }     
        }

        void SmexExchangeSmtpAgent3_OnEndOfData(ReceiveMessageEventSource source, EndOfDataEventArgs e)
        {
            try
            {
                _wrapper.Update(source, e);
            }
            catch (Exception ex)
            {
                _wrapper.ScanCompleted();
                Utilities.Log(ex.ToString());
            }

        }

        public void ScanCompleted()
        {
            _wrapper.ScanCompleted();
        }

        public bool IsCompleted()
        {
            return _wrapper.IsCompleted();
        }

        public string GetEventName()
        {
            return _wrapper.GetEventName();
        }

        public void DisConnect()
        {
            _wrapper.DisConnect();
        }

        public bool IsConnected()
        {
            return _wrapper.IsConnected();
        }

        public bool IsExternalConnection()
        {
            return _wrapper.IsExternalConnection();
        }

        public string GetLastExternalIp()
        {
            return _wrapper.GetLastExternalIp();
        }

        public void RemoveRecipient(string rcpt)
        {
            _wrapper.RemoveRecipient(rcpt);
        }

        public void RejectMessage()
        {
            _wrapper.RejectMessage();
        }

        public void GetMimeContent(out string content)
        {
            _wrapper.GetMimeContent(out content);
        }

        public string GetSubject()
        {
            return _wrapper.GetSubject();
        }

        public HookPoint GetEventType()
        {
            return _wrapper.Hook;
        }

        public SmtpSessionData GetSessionData()
        {
            return _wrapper.GetSessionData();
        }

        public MailItemData GetMailItemData()
        {
            return _wrapper.GetMailItemData();
        }


        public List<string> GetRecipients()
        {
            return _wrapper.GetRecipients();
        }

        public AgentAsyncContext GetOwneredAgentAsyncContext()
        {
            return this.GetAgentAsyncContext();
        }
    }
}
